var tabSlider = document.getElementsByClassName("sliderMaison");
var elmtTab = 0;
function switchSlider(sens){
    if (sens ==  "droite") {
        if (elmtTab+1 == tabSlider.length) {
            tabSlider[elmtTab].style.display = "none";
            elmtTab = 0;
            tabSlider[elmtTab].style.display = "initial";
        }
        else{
            tabSlider[elmtTab].style.display = "none";
            elmtTab+=1;
            tabSlider[elmtTab].style.display = "initial";
        }
    }
    else{
        if (elmtTab == 0) {
            tabSlider[elmtTab].style.display = "none";
            elmtTab = tabSlider.length-1;
            tabSlider[elmtTab].style.display = "initial";
        }
        else{
            tabSlider[elmtTab].style.display = "none";
            elmtTab-=1;
            tabSlider[elmtTab].style.display = "initial";
        }
    }
}