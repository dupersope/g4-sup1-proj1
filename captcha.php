<?php
  $largeur = 110;
  $hauteur = 28;
  $image = imagecreatetruecolor($largeur,$hauteur);
  $color_back = imagecolorallocate($image,00,00,30);
  imagefill($image,0,0,$color_back);
  $texte="";
  for ($i=0; $i < 6; $i++)
  {
    $texte .= chr(rand(65,90))." ";
  }
  session_start();
  $_SESSION["captcha"]=str_replace(" ","",$texte);
  $color_text = imagecolorallocate($image,240,240,240);
  imagestring($image,5,5,5,$texte,$color_text);
  $color_pixel = imagecolorallocate($image,0,0,0);
  for ($i=0; $i < 400; $i++)
  {
    imagesetpixel($image,rand(0,$largeur),rand(0,$hauteur),$color_pixel);
  }
  $color_line = imagecolorallocate($image,45,36,30);
  for ($i=0; $i < 2; $i++)
  {
    imageline($image,rand(0,$largeur),rand(0,$hauteur),rand(0,$largeur),rand(0,$hauteur),$color_line);
  }

  header("content-type:image/png");
  imagepng($image);
 ?>
