<?php
    include "layout.php";


    include "./../constants.php";
    $bdd = new PDO('mysql:host='.$SQL_HOST.';dbname='.$SQL_DBNAME.';charset=utf8', $SQL_USERNAME, $SQL_PASSWORD);
    // Récupération des stats pour les événements
    $req = $bdd->prepare('SELECT COUNT(*) from events WHERE date > NOW()');
    $req->execute();
    $futureEvents = $req->fetch()[0];
    $req = $bdd->prepare('SELECT COUNT(*) from events WHERE date > NOW() AND highlight = true');
    $req->execute();
    $highlightedEvents = $req->fetch()[0];
    $req = $bdd->prepare('SELECT COUNT(*) from places WHERE active = true');
    $req->execute();
    $activePlaces = $req->fetch()[0];
?>

<div class="row">
    <div class="w-12 text-center">
        <h1>Centre d'administration</h1>
    </div>
</div>
<div class="row">
    <div class="w-6 text-center">
        <h2>Événements</h2>
        <h3><?php echo $futureEvents; ?> événements à venir</h3>
        <h3><?php echo $highlightedEvents; ?> événements importants</h3>
        <a href="events.php"><button class="big-backend backend-button">Gérer les événements</button></a>
    </div>

    <div class="w-6 text-center margin-bottom">
        <h2>Lieux importants</h2>
        <h3><?php echo $activePlaces; ?> lieux actifs</h3>
        <h3 style="color: transparent">.</h3>
        <a href="places.php"><button class="big-backend backend-button">Gérer les lieux importants</button></a>
    </div>
</div>
<?php include"./footer.php"; ?>