<?php
session_start();
if(!isset($_SESSION['id'])) {
    header("Location:./../login.php");
}
date_default_timezone_set('Europe/Paris');
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <title> WebMed </title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="./../style.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script> <!-- TODO : déplacer en bas de la page dans le footer -->
    <script src="./../main.js"></script> <!-- TODO : déplacer en bas de la page dans le footer -->
</head>
<body>
    <nav class="topnav" id="myTopnav">
        <a class="listeHeader" href="./../fr/index.php">Voir le site</a>
        <a class="listeHeader" href="index.php">Accueil administration</a>
        <a class="listeHeader" href="events.php">Événements</a>
        <a class="listeHeader" href="places.php">Lieux importants</a>
        <a href="javascript:void(0);" class="icon" onclick="myFunction()">
            <i class="fa fa-bars"></i>
        </a>
    </nav>