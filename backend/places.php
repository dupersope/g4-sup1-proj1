<?php
    include "layout.php";

    include "./../constants.php";
    $bdd = new PDO('mysql:host='.$SQL_HOST.';dbname='.$SQL_DBNAME.';charset=utf8', $SQL_USERNAME, $SQL_PASSWORD);

    if(isset($_GET['deleteId'])) {
        $req = $bdd->prepare('DELETE FROM places  WHERE id = :id');
        $req->execute(array(
                'id' => $_GET['deleteId']
        ));
    }

    if(isset($_GET['changeActiveId'])) {
        $req = $bdd->prepare('UPDATE places SET active = :active  WHERE id = :id');
        $req->execute(array(
            'id' => $_GET['changeActiveId'],
            'active' => $_GET['value']
        ));
    }
    if(isset($_POST['titleFR']) && isset($_POST['descriptionFR']) && isset($_POST['titleEN']) && isset($_POST['descriptionEN'])) {
        $img_seed = $_FILES['photo']['tmp_name'];
        $img_name = uniqid('image_', true);
        $uniqId = uniqid('img_', true);
        mkdir("./../images/$uniqId", 0777, true);

        $img_d = imagecreatefromjpeg($img_seed);
        $chemin = "./../images/$uniqId/$img_name.jpg";
        $cheminToSave = "./images/$uniqId/$img_name.jpg";
        imagejpeg($img_d, $chemin, 100);

        $req = $bdd->prepare('INSERT INTO places(titleFR, descriptionFR, map_linkFR, titleEN, descriptionEN, map_linkEN, active, chemin) VALUES(:titleFR, :descriptionFR, :map_linkFR, :titleEN, :descriptionEN, :map_linkEN, :active, :chemin)');
        $req->execute(array(
            'titleFR' => $_POST['titleFR'],
            'descriptionFR' => $_POST['descriptionFR'],
            'map_linkFR' => $_POST['map_linkFR'],
            'titleEN' => $_POST['titleEN'],
            'descriptionEN' => $_POST['descriptionEN'],
            'map_linkEN' => $_POST['map_linkEN'],
            'active' => (isset($_POST['active'])) ? true : false,
            'chemin' => $cheminToSave
        ));
    }

    $req = $bdd->prepare('SELECT * FROM places');
    $req->execute();
?>

<div class="row">
    <div class="w-12 text-center">
        <h1>Gestion des lieux importants</h1>
    </div>
</div>


<div class="row">
    <div class="w-12">

        <h2>Lieux importants</h2>

        <?php
        while ($donnees = $req->fetch()) { ?>
            <div class="w-12 backend-card">
                <h4><?php echo($donnees['titleFR']); ?> (<?php echo($donnees['titleEN']); ?>)</h4>
                <div class="row">
                    <div class="w-6">
                        <b>Description en français : </b><br />
                        <?php echo($donnees['descriptionFR']); ?>
                    </div>
                    <div class="w-6">
                        <b>Description en anglais : </b><br />
                        <?php echo($donnees['descriptionEN']); ?>
                    </div>
                </div>
                <a class='a-no-style-all' href="places.php?deleteId=<?php echo $donnees['id'] ?>">
                    <button class="backend-button w-6">Supprimer le lieu</button>
                </a>
                <a class='a-no-style-all' href="places.php?changeActiveId=<?php echo $donnees['id']."&value=".!$donnees['active'] ?>">
                    <button class="backend-button w-6"><?php echo ($donnees['active']) ? "Rendre inactif" : "Rendre actif"; ?></button>
                </a>
            </div>

        <?php }
        ?>
        <hr />
    </div>
</div>
<div class="row">
    <form method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="w-12">
                <h2>Ajouter un lieu</h2>
            </div>
            <div class="w-6">
                <h2>Langue française</h2>

                <label for="titleFR">Titre : </label><input required type="text" name="titleFR" id="titleFR" />
                <label for="linkFR">Lien vers la carte : </label><input type="text" name="map_linkFR" id="map_linkFR" />
                <label for="descriptionFR">Description : </label><br />

                <textarea required name="descriptionFR" id="descriptionFR" rows="10" cols="80">

                </textarea>

            </div>
            <div class="w-6">
                <h2>Langue anglaise</h2>

                <label for="titleEN">Titre : </label><input required type="text" name="titleEN" id="titleEN"/>
                <label for="linkEN">Lien vers la carte : </label><input type="text" name="map_linkEN" id="map_linkEN"/>
                <label for="descriptionEN">Description : </label><br/>

                <textarea required name="descriptionEN" id="descriptionEN" rows="10" cols="80">

                </textarea>

            </div>
        </div>
        <div class="row">
            <div class="w-4 text-center padding-top">
                <input type="checkbox" id="active" name="active"
                       value="1" checked /><label for="active"> Lieu actif</label>
            </div>
            <div class="w-4 text-center padding-top">
                <label for="photo">Image : </label>
                <input name="photo" id="photo" type="file" accept="image/jpeg" >
            </div>
            <div class="w-4 text-center padding-top">
                <input type="submit" />
            </div>
        </div>
    </form>
</div>

<script src="../ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'descriptionFR' );
    CKEDITOR.replace( 'descriptionEN' );
</script>
<?php include"./footer.php"; ?>