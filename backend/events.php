<?php
    include "layout.php";

    include "./../constants.php";
    $bdd = new PDO('mysql:host='.$SQL_HOST.';dbname='.$SQL_DBNAME.';charset=utf8', $SQL_USERNAME, $SQL_PASSWORD);
    // Suppression d'un événement
    if(isset($_GET['deleteId'])) {
        $req = $bdd->prepare('DELETE FROM events  WHERE id = :id');
        $req->execute(array(
                'id' => $_GET['deleteId']
        ));
    }

    // Changement du statut actif d'un événement
    if(isset($_GET['changeActiveId'])) {
        $req = $bdd->prepare('UPDATE events SET active = :active  WHERE id = :id');
        $req->execute(array(
            'id' => $_GET['changeActiveId'],
            'active' => $_GET['value']
        ));
    }

    // Changement du statut "highlight" d'une événement
    if(isset($_GET['changeHighlightId'])) {
        $req = $bdd->prepare('UPDATE events SET highlight = :highlight  WHERE id = :id');
        $req->execute(array(
            'id' => $_GET['changeHighlightId'],
            'highlight' => $_GET['value']
        ));
    }

    // Ajout d'un événement
    if(isset($_POST['titleFR']) && isset($_POST['date']) && isset($_POST['descriptionFR']) && isset($_POST['category']) && isset($_POST['titleEN']) && isset($_POST['descriptionEN'])) {
        $img_seed = $_FILES['photo']['tmp_name'];
        $img_name = uniqid('image_', true);
        $uniqId = uniqid('img_', true);
        mkdir("./../images/$uniqId", 0777, true);

        $img_d = imagecreatefromjpeg($img_seed);
        $chemin = "./../images/$uniqId/$img_name.jpg";
        $cheminToSave = "./images/$uniqId/$img_name.jpg";
        imagejpeg($img_d, $chemin, 100);
        
        $req = $bdd->prepare('INSERT INTO events(titleFR, titleEN, date, descriptionFR, descriptionEN, linkFR, linkEN, category, active, highlight, chemin) VALUES(:titleFR, :titleEN, :date, :descriptionFR, :descriptionEN, :linkFR, :linkEN, :category, :active, :highlight, :chemin)');
        $req->execute(array(
            'titleFR' => $_POST['titleFR'],
            'titleEN' => $_POST['titleEN'],
            'date' => $_POST['date'],
            'descriptionFR' => $_POST['descriptionFR'],
            'linkFR' => $_POST['linkFR'],
            'descriptionEN' => $_POST['descriptionEN'],
            'linkEN' => $_POST['linkEN'],
            'category' => $_POST['category'],
            'active' => (isset($_POST['active'])) ? true : false,
            'highlight' => (isset($_POST['highlight'])) ? true : false,
            'chemin' => $cheminToSave
        ));
    }

    // Récupération des événements à afficher
    $req = $bdd->prepare('SELECT * FROM events WHERE date > NOW()');
    $req->execute();
?>

<div class="row">
    <div class="w-12 text-center">
        <h1>Gestion des événements</h1>
    </div>
</div>


<div class="row">
    <div class="w-12">

        <h2>Événements à venir</h2>

        <?php
        while ($donnees = $req->fetch()) { ?>
            <div class="w-12 backend-card">
                <h4><?php echo($donnees['titleFR']); ?> (<?php echo($donnees['titleEN']); ?>)</h4>
                <h5><?php echo($donnees['date']); ?></h5>
                <div class="row">
                    <div class="w-6">
                        <b>Langue française : </b><br />
                        <?php echo($donnees['descriptionFR']); ?>
                    </div>
                    <div class="w-6">
                        <b>Langue anglaise : </b><br />
                        <?php echo($donnees['descriptionEN']); ?>
                    </div>
                </div>
                <a class='a-no-style-all' href="events.php?deleteId=<?php echo $donnees['id'] ?>">
                    <button class="w-4 backend-button">Supprimer l'événement</button>
                </a>
                <a class='a-no-style-all' href="events.php?changeActiveId=<?php echo $donnees['id']."&value=".!$donnees['active'] ?>">
                    <button class="w-4 backend-button"><?php echo ($donnees['active']) ? "Rendre inactif" : "Rendre actif"; ?></button>
                </a>
                <a class='a-no-style-all' href="events.php?changeHighlightId=<?php echo $donnees['id']."&value=".!$donnees['highlight'] ?>">
                    <button class="w-4 backend-button"><?php echo ($donnees['highlight']) ?  "Retirer mise en avant" : "Mettre en avant"; ?></button>
                </a>
            </div>

        <?php }
        ?>
    </div>
</div>
<div class="row">
    <form method="post" class="backend-form"  enctype="multipart/form-data">
        <div class="row">
            <div class="w-12">
                <h2>Ajouter un événement</h2>
            </div>
            <div class="w-6">
                <h2>Langue française</h2>
                <label for="titleFR">Titre : </label><input required type="text" name="titleFR" id="titleFR" />
                <br /><br />
                <label for="linkFR">Lien externe : </label><input type="text" name="linkFR" id="linkFR" />
                <label for="descriptionFR">Description : </label><br />

                <textarea required name="descriptionFR" id="descriptionFR" rows="10" cols="80">

            </textarea>
            </div>
            <div class="w-6">
                <h2>Langue anglaise</h2>
                <label for="titleEN">Titre : </label><input required type="text" name="titleEN" id="titleEN" />
                <br /><br />
                <label for="linkEN">Lien externe : </label><input type="text" name="linkEN" id="linkEN" />
                <label for="descriptionEN">Description : </label><br />

                <textarea required name="descriptionEN" id="descriptionEN" rows="10" cols="80">

            </textarea>
            </div>
            <div class="w-12">
                <label for="category">Catégorie : </label><input required type="text" name="category" id="category" />
                <label for="date">Date : </label><input required type="datetime-local" name="date" id="date" />
            </div>
        </div>
        <div class="row">
            <div class="w-4 text-center padding-top">
                <input type="checkbox" id="active" name="active"
                       value="1" checked /><label for="active"> Événement actif</label>
            </div>
            <div class="w-4 text-center padding-top">
                <input type="checkbox" id="highlight" name="highlight"
                       value="1" /><label for="highlight"> Événement mis en avant</label>
            </div>
            <div class="w-4 text-center padding-top">
                <label for="photo">Image : </label>
                <input name="photo" id="photo" type="file" accept="image/jpeg" >
            </div>
        </div>
        <div class="row">
            <div class="w-12 text-center padding-top">
                <input type="submit" />
            </div>
        </div>
    </form>
</div>

<script src="../ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'description' );
</script>
<?php include"./footer.php"; ?>