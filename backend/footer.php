<div class="footer">
    <div class="row">
        <div class="w-4">
            <a class="a-no-style" href="mailto:contact@webmed.com">
                <i class="fa fa-envelope"></i> contact@webmed.com
            </a>
        </div>
        <div class="w-4">
            <i class="fa fa-map-marker"></i>
            Avenue du Prado Marseille, France
        </div>
        <div class="w-4">
            <a class="a-no-style" href="tel:0412345678">
                <i class="fa fa-phone"></i> 04 12 34 56 78
            </a>
        </div>
    </div>

    <div class="row" id="footer-optional">
        <div class="w-6">
            Webmed © 2018
        </div>
        <?php if(isset($_SESSION['id']) != '') { ?>
            <div class="w-6">
                <a class="a-no-style" href="./../fr/logout.php">Déconnexion</a>
            </div>
        <?php } ?>
    </div>
</div>