<?php session_start(); ?>
<?php include"layout.php"; ?>

    <video id="index-video" width="100%" height="100%" poster="image" autoplay="true", loop="true", muted="true", loop="true"  preload="true", class="no_marge">
        <source src="./../pictures\home-vid-high.mp4" type="video/mp4" />
    </video>

    <div class="row" id="index-history">
        <div class="w-12">
            <a class="anchor" id="histoire"></a>
            <div class="row">
                <div class="w-12 text-center">
                    <h1>Histoire du Prado</h1>
                    <p>
                        Le quartier du Prado se situe dans le 6ème et le 8ème arrondissement de Marseille.
                        Son avenue en forme d’équerre, se situe dans le prolongement de la rue de Rome.
                        Cette avenue s’étend de la place <b>Castellane</b> aux <b>Plages du Prado</b>.
                        La statue de David, un monument historique du quartier est une réplique du célèbre « David » de Michel-Ange.<br />
                        Fort de son histoire, le quartier répertorie quelque bâtiments célèbres comme on peut le voir sur la rue du Prado :
                        <br />
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="w-6">
                    <ul>
                        <li>
                            <span class="li-header">Au N° 320 : </span><br />
                            <b>Le Grand Pavois</b> un immeuble de grande hauteur.
                        </li>
                        <li>
                            <span class="li-header">Au N° 555 : </span><br />
                            L’ancienne <b>maison</b> du grand photographe <b>Nadar</b>, aujourd'hui occupée par le Conseil départemental de l'ordre des médecins des Bouches du Rhône.
                        </li>
                        <li>
                            <span class="li-header">Au N° 81 : </span><br />
                            La Basilique du Sacré-Cœur.
                        </li>
                        <li>
                            <span class="li-header">Au N° 36 : </span><br />
                            Le cinéma Gaumont dont la façade présente un des rares exemples d'art nouveau à Marseille.
                        </li>
                    </ul>
                </div>
                <div class="w-6">
                    <img class="index-img" src="./../pictures/bsc.jpg" alt="Le cinéma du Prado" style="width: 90%; margin-left: 5%;"/>
                </div>
            </div>
        </div>
        <div class="w-12">
            <div class="row">
                <div class="w-6">
                    <img class="index-img" src="./../pictures/corbusier.jpg" alt="Le Corbusier" style="width: 90%; margin-left: 5%;">
                </div>
                <div class="w-6">
                    <p class="no-margin">Ou encore sur le Boulevard Michelet :</p>
                    <ul>
                        <li>
                            <span class="li-header">Au N° 280 : </span><br />
                            La Cité Radieuse, véritable village vertical portant le nom : <b>Le Corbusier</b>, du nom de son illustre architecte.
                        </li>
                       <li>
                           <span class="li-header">Au N° 245 : </span><br />
                            Un lieu réputé pour son ancienneté est le Jardin de <b>la Magalone</b>.
                            Il abrite une bastide provençale typique construite entre 1790 et 1810. Il est indiqué dans le registre des monuments historiques depuis 1948.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <?php
        // Connexion à la BDD
        include "./../constants.php";
        $bdd = new PDO('mysql:host=' . $SQL_HOST . ';dbname=' . $SQL_DBNAME . ';charset=utf8', $SQL_USERNAME, $SQL_PASSWORD);

        // Récupération des événements
        $reqRecupInfo = $bdd->prepare("SELECT * FROM places WHERE  active = 1;");
        $reqRecupInfo->execute();

        // S'il y a des événements, on les affiche
        if ($reqRecupInfo->rowCount() > 0) { $i = 0;?>

            <div class="row" id="index-main-places">
                <a class="anchor" id="lieux-importants"></a>
                <div class="w-12 text-center">
                    <h1>Lieux importants</h1>

                    <div id="sliderGlobal">
                    <div>

            <?php while ($row = $reqRecupInfo->fetch()) { ?>

                        <div class="sliderMaison <?php if($i == 0) echo 'starter'; ?>">
                            <h3><?= $row['titleFR']; ?> :</h3>
                            <div class="imageSlide">
                                <div class="img-container" style="background-image: url('./../<?= $row['chemin'] ?>');">
                                        <i style="position: absolute; left: 25px; transform: translate(0, 50%); bottom: 50%;" class="slider-angle fa fa-angle-left" onclick='switchSlider("gauche")'></i>
                                        <i style="position: absolute; right: 25px; transform: translate(0, 50%); bottom: 50%;" class="slider-angle fa fa-angle-right" onclick='switchSlider("droite")'></i>
                                </div>
                                <blockquote style="min-height: 120px;">
                                    <?= $row['descriptionFR'] ?>
                                </blockquote>
                            </div>
                        </div>

            <?php $i++; } ?>

                    </div>
                    </div>
                </div>
            </div>

    <?php
        }
    ?>

    <div class="row">
        <?php include"maps.php"; ?>
    </div>
<?php include"footer.php"; ?>