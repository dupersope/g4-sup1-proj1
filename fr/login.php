<?php
    session_start();
    $error = false;
    $captchaError = false;
    if(isset($_POST['password']) && isset($_POST['pseudo'])) {
        try {
            if (strtoupper($_POST["captcha"])!=$_SESSION["captcha"])
            {
                $captchaError = true;
            }
            else {
                include "./../constants.php";
                $bdd = new PDO('mysql:host=' . $SQL_HOST . ';dbname=' . $SQL_DBNAME . ';charset=utf8', $SQL_USERNAME, $SQL_PASSWORD);

                $req = $bdd->prepare('SELECT id, password FROM user WHERE pseudo = :pseudo');
                $req->execute(array(
                    'pseudo' => $_POST['pseudo']));
                $resultat = $req->fetch();
                $isPasswordCorrect = $resultat[1] == md5($_POST['password']);
                if ($isPasswordCorrect) {
                    $_SESSION['id'] = $resultat[0];
                    $_SESSION['pseudo'] = $_POST['pseudo'];
                    header("Location:./../backend/index.php");

                } else {
                    $error = true;
                }
            }
        } catch (Exception $e) {
            echo("erreur");
            die('Erreur : ' . $e->getMessage());
        }
    }

?>

<?php include"layout.php"; ?>


<div class="row">
    <div class="w-12 text-center">
        <h1>Connexion au centre d'administration</h1>
    </div>
    <?php if($error) { ?>
        <div class="w-12 text-center">
            <div class="error">Erreur de connexion !</div>
        </div>
    <?php } ?>
    <?php if($captchaError) { ?>
        <div class="w-12 text-center">
            <div class="error">Captcha incorrect !</div>
        </div>
    <?php } ?>
    <form method="POST" class="w-12">
        <div class="w-12 text-center">
            <label for="pseudo">Pseudo : </label><br />
            <input class="login-input" type="text" id="pseudo" name="pseudo" />
        </div>
        <div class="w-12 text-center">
            <label for="password">Mot de passe : </label><br />
            <input class="login-input" type="password" id="password" name="password" />
        </div>
        <div class="w-12 text-center">
            <label for=captcha id="labelCaptch"><img id="imageCaptcha" src=./../captcha.php></label>
            <input id="inputCaptcha" class="login-input" type=text name=captcha placeholder="Captcha" required>
        </div>
        <div class="w-12 text-center">
            <input type="submit" value="Connexion"/>
        </div>
    </form>
</div>
<?php include"footer.php"; ?>
