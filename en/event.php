<?php session_start(); ?>
<?php include "layout.php"; ?>
<div class="row">
    <div class="w-12">
        <?php
        // Connexion à la BDD
        include "./../constants.php";
        $bdd = new PDO('mysql:host=' . $SQL_HOST . ';dbname=' . $SQL_DBNAME . ';charset=utf8', $SQL_USERNAME, $SQL_PASSWORD);

        // Récupération des événements
        $reqRecupInfo = $bdd->prepare("SELECT * FROM events WHERE date > CURDATE() - INTERVAL 2 DAY AND active = 1 ORDER BY highlight desc, date;");
        $reqRecupInfo->execute();

        // S'il y a des événements, on les affiche
        if ($reqRecupInfo->rowCount() > 0) {
            ?>

            <h1 class="text-center">Prado events</h1>

            <?php
            $i = 0;
            while ($row = $reqRecupInfo->fetch()) {
                $i++;
                ?>
                <div class="row event-card">
                    <?php if ($i % 2 != 0) { ?>
                        <div class="event-img-container w-6 text-center">
                            <?php if($row['chemin']) { ?>
                                <img class="event-img" alt="<?= $row['titleEN'] ?>" src="./../<?php echo $row['chemin']; ?>">
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <div class="w-6">
                        <span><?= strftime('%Y/%m/%d - %H:%M', strtotime($row['date'])); ?></span>
                        <h2><?= $row['titleEN']; ?></h2>
                        <p><?= $row['descriptionEN']; ?></p>
                        <button class="w-6 backend-button">
                            <a target="_blank" href="<?= $row['linkEN']; ?>">See more...</a>
                        </button>
                    </div>
                    <?php if ($i % 2 == 0) { ?>
                        <div class="event-img-container w-6 text-center">
                            <?php if($row['chemin']) { ?>
                                <img class="event-img" alt="<?= $row['titleEN'] ?>" src="./../<?php echo $row['chemin']; ?>">
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <?php
            }
        } else { ?>
            <div class="row">
                <div class="w-12 text-center">
                    <h2>Pas d'événements à venir...</h2>

                    <h4>Revenez plus tard !</h4>
                </div>
            </div>
        <?php } ?>
    </div>
</div>


<?php include"footer.php"; ?>