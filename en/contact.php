<?php session_start(); ?>
<?php include"layout.php";

$errorCaptcha = false;
if(isset($_POST['nom']) && isset($_POST['email']) && isset($_POST['message']) && isset($_POST['captcha'])) {
    echo "Message submitted";
    if (strtoupper($_POST["captcha"])!=$_SESSION["captcha"]) {
        $errorCaptcha = true;
    } else {
        $email_from = "leprado@webmed.com";
        $email_to = "yoann.bohssain@gmail.com";
        $email_subject = "LePrado - Demande de contact";

        $email_message = "Sujet : " . $_POST['sujet'] . "\r\n";
        $email_message .= "Contact : " . $_POST['nom'] . " : " . $_POST['email'] . "\r\n";
        $email_message .= "Message : \r\n" . $_POST['message'];

        $headers = 'From: '.$email_from."\r\n".
            'Reply-To: '.$email_from."\r\n" .
            'X-Mailer: PHP/' . phpversion();
        @mail($email_to, $email_subject, $email_message, $headers);

        $email_to = $_POST['email'];
        $email_subject = "ThePrado - Contact request";

        $email_message = "Hello " . $_POST['nom'] . ",\r\nYour message has been sent and will be processed as soon as possible.\r\n";
        $email_message .= "The Prado team thank you, see you soon !";

        $headers = 'From: '.$email_from."\r\n".
            'Reply-To: '.$email_from."\r\n" .
            'X-Mailer: PHP/' . phpversion();
        @mail($email_to, $email_subject, $email_message, $headers);
    }
}
?>

<div class="container">
  <form id="contact" action="" method="post">
      <?php if($errorCaptcha) { ?>
          <h4 class="text-center error">Captcha error !</h4>
      <?php } ?>
    <h3>Contact us !</h3>
    <h4>Have you any question ?</h4>
    <fieldset>
      <input placeholder="Name" type="text" name="nom" required>
    </fieldset>
    <fieldset>
      <input placeholder="Email" type="email" name="email" required>
    </fieldset>
    <fieldset>   
        <div >
          <select name="sujet" required style="background-color: #F48C64; color: #ffffff;">
              <option value="Notification d'évènements à venir">In coming events</option>
              <option value="Création de commerce">Registering shop</option>
              <option value="Autre">Other</option>
          </select>
        </div>
    </fieldset>
    <fieldset>
      <textarea placeholder="Your message" name='message' required></textarea>
    </fieldset>
    <fieldset>
        <label for=captcha id="labelCaptch"><img id="imageCaptcha" src=./../captcha.php></label>
        <input id="inputCaptcha" class="login-input" type=text name=captcha placeholder="Captcha" required>
    </fieldset>
    <fieldset>
      <button name="submit" type="submit" id="contact-submit">Send</button>
    </fieldset>
  </form>
</div>
<?php include"footer.php"; ?>