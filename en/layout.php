<?php
if(!isset($_SESSION)) {
    header("Location:./login.php");
}
date_default_timezone_set('Europe/Paris');

?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <title> The Prado </title>
    <link rel="icon" type="image/png" href="./../pictures/logo.png" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">  
    <link rel="stylesheet" href="./../style.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script> <!-- TODO : déplacer en bas de la page dans le footer -->
    <script src="./../main.js"></script> <!-- TODO : déplacer en bas de la page dans le footer -->
    <script src="./../slider.js"></script> <!-- TODO : déplacer en bas de la page dans le footer -->
</head>

<body>
    <nav class="topnav" id="myTopnav">
            <a href="index.php" id="logo"><img style="height:40px;" src="./../pictures/logo.png" alt="logo" /></a>
            <a class="listeHeader" href="index.php">Home</a>
            <a href="index.php#histoire" class="listeHeader">History</a>
            <a href="index.php#lieux-importants" class="listeHeader">Main places</a>
            <a href="event.php" class="listeHeader">Events</a>
            <a href="contact.php" class="listeHeader" >Contact</a>
            <a style='position: absolute; right: 0px' href="./../fr/index.php" class="listeHeader" >
                <img style="height: 24px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAAAvCAYAAABe1bwWAAABlElEQVRoQ+3bv07CUBTH8d/BQYluYpx8D0cXWxYHTZCS+DcODj6MM5oIiQ6WKiRO+gY8isS4OACLHENDSUUlnFiGm/yYCFxK7iffnulWkHr5lUYRAz2HYBPAOgBJf29936ztWn8y8/r+0cXMa6csVEA7AmkrctXVqPqcrI03vnVSW1rs5+uAlLP4t+QaDsBMbFfCLpZPN6LLXgzjBeGtAAdZogyv5R4MoCp3hYerQ9ku33s5kZesUVyFiR1UfPGDxiOge4T5JtASPwjfABQIkxbQ1yGMzgPF6VsJGBDmjyoIQxjbwGAxLIbF2ARYjM2LM4bFsBibAIuxeXHGsBgWYxNgMTYvzhgWw2JsAizG5sUZw2JYjE2Axdi8OGNYDIuxCbAYmxdnDIthMTYBFmPz4oxhMSzGJjClGJ7B+4Gj7+IFYVOAuRzhdvGc78ioxXO+v91KgiJPhk/AjE+GDz8vlRr5jwWtA9jPZHKNLuLcrSSIurpyPH6WIMHwKuGOqJwBmjx98i8nR2A6ArQ/gZu16Pop2fAXkg44lBfMKAsAAAAASUVORK5CYII=" />
            </a>
            <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                <i class="fa fa-bars"></i>
            </a>
    </nav>
    <div class="navbar-catchup"></div>