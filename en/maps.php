<div class="w-12 text-center" id="index-maps">
         <!-- Restaurant -->
         	<div id="Restaurant" class="w-12 map-responsive">
         		<h1>Restaurant</h1>
				<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d6819.74460528402!2d5.384738538614452!3d43.26213603899716!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sRestaurants!5e0!3m2!1sfr!2sfr!4v1537448061286" height="500" width="800" frameborder="0" class="cartes" style="border:0" allowfullscreen></iframe>
	         </div>
	         <!-- Hotel -->
	         <div id="Hotel" class="w-12 map-responsive">
	         	<h1>Hotels</h1>
	         	<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d8145.5702438629205!2d5.382745037157874!3d43.264420575890114!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sprado+marseille+hotel!5e0!3m2!1sfr!2sfr!4v1537448514205"  height="500" width="800" frameborder="0" class="cartes" style="border:0" allowfullscreen></iframe>
	         </div>
	         <!-- Bar -->
	         <div id="Bar" class="w-12 map-responsive">
	         	<h1>Pubs</h1>
	         	<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d7720.286670365623!2d5.381389757819378!3d43.265711883407015!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sprado+marseille+bar!5e0!3m2!1sfr!2sfr!4v1537448759393"  height="500" width="800" frameborder="0" class="cartes" style="border:0" allowfullscreen></iframe>
	     	</div>
	         <!-- Lieux important -->
	         <div id="LI" class="w-12 map-responsive">
	         	<h1>Places to see</h1>
	         	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6940.602796377728!2d5.3821637633210555!3d43.26532962947522!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12c9b8a778a71c19%3A0xf30ae56cc3f4dc1e!2sOrange+V%C3%A9lodrome!5e0!3m2!1sfr!2sfr!4v1537449320756"  height="500" width="800" frameborder="0" class="cartes" style="border:0" allowfullscreen></iframe>
	         </div>
	         <!-- Boutique -->
	         <div id="Boutique" class="w-12 map-responsive">
	         	<h1>Shops</h1>
	         	<iframe src="https://www.google.com/maps/embed?pb=!1m12!1m8!1m3!1d8145.4625130732475!2d5.384210336064815!3d43.26531121446817!3m2!1i1024!2i768!4f13.1!2m1!1sprado+marseille+boutique!5e0!3m2!1sfr!2sfr!4v1537449830972"  height="500" width="800" frameborder="0" class="cartes" style="border:0" allowfullscreen></iframe>
	         </div>
	         <!-- Transport -->
	         <div id="Transport" class="w-12 map-responsive">
	         	<h1>Publics Transport</h1>
	         	<iframe src="https://www.google.com/maps/embed?pb=!1m12!1m8!1m3!1d8290.423510910634!2d5.383450210508028!3d43.264036219860614!3m2!1i1024!2i768!4f13.1!2m1!1sprado+marseille+transport!5e0!3m2!1sfr!2sfr!4v1537449634288"  height="500" width="800" frameborder="0" class="cartes" style="border:0" allowfullscreen></iframe>
	         </div>
	         <!-- Banque -->
	         <div id="Banque" class="w-12 map-responsive">
	         	<h1>Banks</h1>
	         	<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d8145.462858821401!2d5.385566732938387!3d43.26523066475987!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sprado+marseille+banque!5e0!3m2!1sfr!2sfr!4v1537449750840"  height="500" width="800" frameborder="0" class="cartes" style="border:0" allowfullscreen></iframe>
	         </div>
    <div class="center" style="margin-top: 20px;">
        <button class="boutonCarte" onclick="afficheCarte('restaurant')">Restaurants</button>
        <button class="boutonCarte" onclick="afficheCarte('hotel')">Hotels</button>
        <button class="boutonCarte" onclick="afficheCarte('bar')">Pubs</button>
        <button class="boutonCarte" id="LI-button" onclick="afficheCarte('LI')">Places to see</button>
        <button class="boutonCarte" onclick="afficheCarte('boutique')">Shops</button>
        <button class="boutonCarte" onclick="afficheCarte('transport')"> Publics Transport</button>
        <button class="boutonCarte" onclick="afficheCarte('banque')">Banks</button>
    </div><br>
    <script type="text/javascript">
    	function afficheCarte(categ){
    		switch(categ){
    			case 'restaurant':
    				document.getElementById("Restaurant").style.display = "initial";
    				document.getElementById("LI").style.display = "none";
    				document.getElementById("Hotel").style.display = "none";
    				document.getElementById("Bar").style.display = "none";
    				document.getElementById("Boutique").style.display = "none";
    				document.getElementById("Transport").style.display = "none";
    				document.getElementById("Banque").style.display = "none";
    				break;
    			case 'hotel':
    				document.getElementById("Restaurant").style.display = "none";
    				document.getElementById("LI").style.display = "none";
    				document.getElementById("Hotel").style.display = "initial";
    				document.getElementById("Bar").style.display = "none";
    				document.getElementById("Boutique").style.display = "none";
    				document.getElementById("Transport").style.display = "none";
    				document.getElementById("Banque").style.display = "none";
    				break;
    			case 'bar':
    				document.getElementById("Restaurant").style.display = "none";
    				document.getElementById("LI").style.display = "none";
    				document.getElementById("Hotel").style.display = "none";
    				document.getElementById("Bar").style.display = "initial";
    				document.getElementById("Boutique").style.display = "none";
    				document.getElementById("Transport").style.display = "none";
    				document.getElementById("Banque").style.display = "none";
    				break;
    			case 'LI':
    				document.getElementById("Restaurant").style.display = "none";
    				document.getElementById("LI").style.display = "initial";
    				document.getElementById("Hotel").style.display = "none";
    				document.getElementById("Bar").style.display = "none";
    				document.getElementById("Boutique").style.display = "none";
    				document.getElementById("Transport").style.display = "none";
    				document.getElementById("Banque").style.display = "none";
    				break;
    			case 'boutique':
    				document.getElementById("Restaurant").style.display = "none";
    				document.getElementById("LI").style.display = "none";
    				document.getElementById("Hotel").style.display = "none";
    				document.getElementById("Bar").style.display = "none";
    				document.getElementById("Boutique").style.display = "initial";
    				document.getElementById("Transport").style.display = "none";
    				document.getElementById("Banque").style.display = "none";
    				break;
    			case 'transport':
    				document.getElementById("Restaurant").style.display = "none";
    				document.getElementById("LI").style.display = "none";
    				document.getElementById("Hotel").style.display = "none";
    				document.getElementById("Bar").style.display = "none";
    				document.getElementById("Boutique").style.display = "none";
    				document.getElementById("Transport").style.display = "initial";
    				document.getElementById("Banque").style.display = "none";
    				break;
    			case 'banque':
    				document.getElementById("Restaurant").style.display = "none";
    				document.getElementById("LI").style.display = "none";
    				document.getElementById("Hotel").style.display = "none";
    				document.getElementById("Bar").style.display = "none";
    				document.getElementById("Boutique").style.display = "none";
    				document.getElementById("Transport").style.display = "none";
    				document.getElementById("Banque").style.display = "initial";
    				break;
    		}
    	}
    </script>
</div>
