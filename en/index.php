<?php session_start(); ?>
<?php include"layout.php"; ?>

    <video id="index-video" width="100%" height="100%" poster="image" autoplay="true", loop="true", muted="true", loop="true"  preload="true", class="no_marge">
        <source src="./../pictures/home-vid-en-high.mp4" type="video/mp4" />
    </video>

    <div class="row" id="index-history">
        <div class="w-12">
            <a class="anchor" id="histoire"></a>
            <div class="row">
                <div class="w-12 text-center">
                    <h1>Prado’s History</h1>
                    <p>
                        The Prado’s district is in the 6th and 8th borough of Marseille.
                        Its main street is bended around the prado’s roundabout.
                        It connects <b>Castellane square</b> and  <b>Prado’s beach</b>.
                        The David’s statue, an historical monument of the site is a copy of Michelangelo’s famous « <b>David</b> ».<br>
                        As we can see, the district harbors some prestigious buildings inherited of its history.
                        On the Prado street :
                        <br />
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="w-6">
                    <ul>
                        <li>
                            <span class="li-header">At the number 320 : </span><br />
                            <b>Le Grand Pavois</b> a tall building.
                        </li>
                        <li>
                            <span class="li-header">At the number 555 : </span><br />
                            The former <b>house</b> of the great photographer <b>Nadar</b>, occupied today by the departmental council and the doctor’s order of the Bouches du Rhône.
                        </li>
                        <li>
                            <span class="li-header">At the number 81 : </span><br />
                            The <b>Sacré-Cœur</b> basilica.
                        </li>
                        <li>
                            <span class="li-header">At the number 36 : </span><br />
                            Gaumont Cinema with his new art frontage, one of the few examples in Marseille.
                        </li>
                    </ul>
                </div>
                <div class="w-6">
                    <img class="index-img" src="./../pictures/bsc.jpg" alt="Le cinéma du Prado" style="width: 90%; margin-left: 5%;"/>
                </div>
            </div>
        </div>
        <div class="w-12">
            <div class="row">
                <div class="w-6">
                    <img class="index-img" src="./../pictures/corbusier.jpg" alt="Le Corbusier" style="width: 90%; margin-left: 5%;">
                </div>
                <div class="w-6">
                    <p class="no-margin">Or on Michelet Boulevard :</p>
                    <ul>
                        <li>
                            <span class="li-header">Au N° 280 : </span><br />
                            The radiant city, «<b> Le Corbusier</b> », vertical village named after its architect.
                        </li>
                       <li>
                           <span class="li-header">Au N° 245 : </span><br />
                           « La Magalone »</b> is a park famous for its great age.
                           It hosts a typical Provençal farmhouse built between 1790 and 1810. In the historical monument register since 1948.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <?php
        // Connexion à la BDD
        include "./../constants.php";
        $bdd = new PDO('mysql:host=' . $SQL_HOST . ';dbname=' . $SQL_DBNAME . ';charset=utf8', $SQL_USERNAME, $SQL_PASSWORD);

        // Récupération des événements
        $reqRecupInfo = $bdd->prepare("SELECT * FROM places WHERE  active = 1;");
        $reqRecupInfo->execute();

        // S'il y a des événements, on les affiche
        if ($reqRecupInfo->rowCount() > 0) { $i = 0;?>

            <div class="row" id="index-main-places">
                <a class="anchor" id="lieux-importants"></a>
                <div class="w-12 text-center">
                    <h1>Main places</h1>

                    <div id="sliderGlobal">
                    <div>

                        <?php while ($row = $reqRecupInfo->fetch()) { ?>

                            <div class="sliderMaison <?php if($i == 0) echo 'starter'; ?>">
                                <h3><?= $row['titleEN']; ?> :</h3>
                                <div class="imageSlide">
                                    <div class="img-container" style="background-image: url('./../<?= $row['chemin'] ?>');">
                                        <i style="position: absolute; left: 25px; transform: translate(0, 50%); bottom: 50%;" class="slider-angle fa fa-angle-left" onclick='switchSlider("gauche")'></i>
                                        <i style="position: absolute; right: 25px; transform: translate(0, 50%); bottom: 50%;" class="slider-angle fa fa-angle-right" onclick='switchSlider("droite")'></i>
                                    </div>
                                    <blockquote style="min-height: 120px;">
                                        <?= $row['descriptionEN'] ?>
                                    </blockquote>
                                </div>
                            </div>

                            <?php $i++; } ?>

                    </div>
                    </div>
                </div>
            </div>

    <?php
        }
    ?>

    <div class="row">
        <?php include"maps.php"; ?>
    </div>
<?php include"footer.php"; ?>